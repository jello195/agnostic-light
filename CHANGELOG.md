# Changelog
All notable changes to this project will be documented in this file.

## [1.0.4] - 2024-12-10
### Added
- The ability to chose settings for every light model
- consistency when closing/opening light model with a token
### Changed
- pointed to new light naming convention for v12
### Removed
- Dependencies in the module.json

## [1.0.3] - 2024-07-02
### Initial commit
- Adds a light on token
- Adds a light panel
- Adds a vision panel
- Adds customization options to the main light
- All the above features can be allowed for GM only