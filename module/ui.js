Hooks.on('getSceneControlButtons', controls => {
	// Va chercher les settings d'agnostic light	
	const lightIcon = game.settings.get("agnostic-light", "lightIcon");
	const lightPresets = game.settings.get("agnostic-light", "lightPresets");
	const al_vision = game.settings.get("agnostic-light", "al_vision");
	
	// Configure l'icone selon les préfèrences
	let fiat_lux_txt = "fa-solid fa-fire-smoke";
	
	if (lightIcon=="feu"){
		fiat_lux_txt = "fa-solid fa-fire-smoke";
	} else if (lightIcon=="lampe") {
		fiat_lux_txt = "fa-solid fa-lightbulb-on";
	}
	vision_txt = "fa-solid fa-eye-low-vision";
	
	
	const lightingControls = controls.find(control => control.name === "token");
	if (lightingControls) {
		if (lightPresets || game.user.isGM) {
			// Ajouter un bouton personnalisé à la catégorie 'lighting'
			lightingControls.tools.push({
				name: game.i18n.localize("agnostic-light.ui.fiatLux"),
				title: game.i18n.localize("agnostic-light.ui.fiatLux_hint"),
				icon: fiat_lux_txt,
				onClick: () => {
					let dialogEditor = new Dialog({
						title: game.i18n.localize("agnostic-light.ui.fiatLux"),
						buttons: (() => {
							// Créer un objet pour les lumières
							const buttons = Object.keys(AgnosticLight.models).reduce((acc, key) => {
								acc[key] = {
									label: game.i18n.localize(`agnostic-light.ui.${key}`),
									callback: () => {
										canvas.tokens.controlled.forEach((token) => {
											backupTokensLight(token); // Sauvegarder les lumières existantes
											AgnosticLight.applyLight(token, key); // Appliquer la lumière
											token.document.setFlag('agnostic-light', 'chosenModel', key); // Mémoriser le modèle choisi
										});
										dialogEditor.render(true);
									},
								};
								return acc;
							}, {});

							// Ajouter le bouton "none" avec une classe spéciale
							buttons.none = {
								label: game.i18n.localize("agnostic-light.ui.fiatLux_none"),
								callback: () => {
									canvas.tokens.controlled.forEach((token) => {
										AgnosticLight.resetLight(token);
									});
									dialogEditor.render(true);
								},
								icon: null,
							};

							// Ajouter le bouton "close" avec une classe spéciale
							buttons.close = {
								icon: '<i class="fa-solid fa-circle-xmark"></i>',
								label: game.i18n.localize("agnostic-light.ui.exit"),
								callback: () => {}, // Fermer le dialogue
							};

							return buttons;
						})(),
						default: "close",
						close: () => {},
						render: (html) => {},
					});

					dialogEditor.render(true);
				},
				button: true,
			});
		}
		// Ajoute des contrôles pour la vision
		if (al_vision || game.user.isGM) {
		  lightingControls.tools.push({
			name: "al_vision",
			title: game.i18n.localize("agnostic-light.ui.visiontitle"),
			icon: vision_txt,
			onClick: () => {
			  let dialogEditor = new Dialog({
				title: game.i18n.localize("agnostic-light.ui.vision"),
				buttons: {
				  none: {
					label: game.i18n.localize("agnostic-light.ui.normal"),
					callback: () => {
					  canvas.tokens.controlled.forEach(async token => {
						// Vision normale : on active la vision, avec une portée de 18, mode "basic"
						await token.document.update({
						  "sight.enabled": true,
						  "sight.range": 18,
						  "sight.visionMode": "basic"
						});
					  });
					  dialogEditor.render(true);
					}
				  },
				  blind: {
					label: game.i18n.localize("agnostic-light.ui.blinded"),
					callback: () => {
					  canvas.tokens.controlled.forEach(async token => {
						// Aveuglé : on désactive la vision
						await token.document.update({
						  "sight.enabled": false,
						  "sight.range": 0,
						  "sight.visionMode": "basic"
						});
					  });
					  dialogEditor.render(true);
					}
				  },
				  noir: {
					label: game.i18n.localize("agnostic-light.ui.inDark"),
					callback: () => {
					  canvas.tokens.controlled.forEach(async token => {
						// Dans le noir total mais vision activée : on met range à 0,
						// l'environnement devra fournir de la lumière (visionMode=basic).
						await token.document.update({
						  "sight.enabled": true,
						  "sight.range": 0,
						  "sight.visionMode": "basic"
						});
					  });
					  dialogEditor.render(true);
					}
				  },
				  close: {
					icon: '<i class="fa-solid fa-circle-xmark"></i>',
					label: game.i18n.localize("agnostic-light.ui.exit")
				  },
				},
				default: "close",
				close: () => {}
			  });
			  dialogEditor.render(true);
			},
			button: true
		  });
		};
	}
});

function backupTokensLight(token){
	let state = token.document.getFlag('agnostic-light', 'lightIconState') || 'off';
	
	if (state=="off"){
		let currentLightningInfo = token.document.light;
		let oldLight = {
			bright: currentLightningInfo.bright,
			dim: currentLightningInfo.dim,
			angle: currentLightningInfo.angle,
			color: currentLightningInfo.color,
			intensity: currentLightningInfo.intensity,
			animation: {
			  type: currentLightningInfo.animation.type,
			}
		};
		token.document.setFlag('agnostic-light', 'base_light', oldLight);
	}
};

Hooks.on('renderTokenHUD', (hud, html, data) => {
	const tokenButton = game.settings.get("agnostic-light", "tokenButton");
	if(tokenButton || game.user.isGM){
		// Hook to add a button to the token HUD
		const lightIcon = game.settings.get("agnostic-light", "lightIcon");

		let token = canvas.tokens.get(data._id);
		let state = token.document.getFlag('agnostic-light', 'lightIconState') || 'off';
		
		//console.log("agnostic-light",state);
		
		let fire;
		let moon;
		
		if (state=='off'){
			fire = "initial";
			moon = "none";
		} else{
			fire = "none";
			moon = "initial";
		}
		
		let btn;
		
		if (lightIcon=="feu"){
			btn = $(
				`<div class="control-icon al-icon" style="display:${fire};">
					<i class="fa-regular fa-fire"></i>
				</div>`
			);
			btn2 = $(
			`<div class="control-icon al-icon" style="display:${moon};">
				<i class="fa-solid fa-moon"></i>
			</div>`
			);
		} else if (lightIcon=="lampe") {
			btn = $(
				`<div class="control-icon al-icon" style="display:${fire};">
					<i class="fa-solid fa-flashlight"></i>
				</div>`
			);
			btn2 = $(
			`<div class="control-icon al-icon" style="display:${moon};">
				<i class="fa-solid fa-lightbulb-slash"></i>
			</div>`
			);
		}

		// Add an event listeners to the buttons
		btn.on('click', () => {
			lightson(data._id);
			token.document.setFlag('agnostic-light', 'lightIconState', 'on');
		});
		
		btn2.on('click', () => {
			lightsoff(data._id);
			token.document.setFlag('agnostic-light', 'lightIconState', 'off');
		});

		// Append the button to the token HUD
		html.find('.col.left').append(btn);
		html.find('.col.left').append(btn2);
	};
});

function lightsoff(tokenId) {
    let token = canvas.tokens.get(tokenId);

    if (token) {
        try {
            AgnosticLight.resetLight(token); // Réinitialise avec la logique centralisée
            console.log(`agnostic-light | light off`);
        } catch (error) {
            console.error(`agnostic-light | Error resetting light for token:`, error);
        }
    } else {
        console.error(`agnostic-light | Token with ID ${tokenId} not found.`);
    }
}


function lightson(tokenId, defaultModel = "torchLight") {
    let token = canvas.tokens.get(tokenId);

    if (token) {
        // Vérifier si un modèle est déjà mémorisé pour ce token
        let chosenModel = token.document.getFlag('agnostic-light', 'chosenModel');

        // Si aucun modèle n’est mémorisé, on utilise le modèle par défaut
        if (!chosenModel) {
            chosenModel = defaultModel;
        }

        // Sauvegarde la lumière actuelle avant modification
        let currentLightningInfo = token.document.light;
        let oldLight = {
            bright: currentLightningInfo.bright,
            dim: currentLightningInfo.dim,
            angle: currentLightningInfo.angle,
            color: currentLightningInfo.color,
            alpha: currentLightningInfo.alpha,
            animation: {
                type: currentLightningInfo.animation?.type,
            },
        };
        token.document.setFlag('agnostic-light', 'base_light', oldLight);

        try {
            // Applique les réglages du modèle mémorisé
            AgnosticLight.applyLight(token, chosenModel);
            console.log(`agnostic-light | light on with settings from ${chosenModel}`);
        } catch (error) {
            console.error(`agnostic-light | Error applying light for token:`, error);
        }
    } else {
        console.error(`agnostic-light | Token with ID ${tokenId} not found.`);
    }
}
