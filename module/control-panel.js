class ControlPanel extends FormApplication {
  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: "control-panel",
      title: game.i18n.localize("agnostic-light.controlPanel.title"),
      template: "modules/agnostic-light/templates/control-panel.hbs",
      width: 450,
      height: 400,
      closeOnSubmit: true
    });
  }

  getData(options) {
		const models = Object.entries(AgnosticLight.models).map(([key, model]) => ({
			key,
			...model,
		  }));

		  const selectedLightKey = this.options.selectedLightKey || "torchLight";
		  const selectedModel = models.find((model) => model.key === selectedLightKey);

		  return {
			models,
			selectedLightKey,
			selectedModel,
			vision: game.settings.get("agnostic-light", "al_vision") // si besoin
		  };
	}
  
  
  
  activateListeners(html) {
		super.activateListeners(html);

	  // Quand on change de sélection de modèle
	  html.find("#light-select").change((event) => {
		const selectedKey = event.target.value;
		const model = AgnosticLight.models[selectedKey];

		// Mettre à jour les champs du formulaire selon le modèle choisi
		html.find("#al-color").val(model.color);
		html.find("#al-dim").val(model.dim);
		html.find("#al-bright").val(model.bright);
		html.find("#al-angle").val(model.angle);
		html.find("#al-intensity").val(model.intensity);
		html.find("#al-intensity-value").text(model.intensity);

		// Mettre à jour l'animation
		const animationSelect = html.find('#al-animation');
		animationSelect.val(model.animation?.type || "none");
	  });

	  // Écouteur pour l'intensité en direct
	  html.find('input[type="range"]').on('input', (event) => {
		const target = event.target;
		html.find(`#${target.id}-value`).text(target.value);
	  });

	  // Remplissage dynamique du menu déroulant des animations au chargement initial
	  const animationSelect = html.find('#al-animation');
	  const animations = Object.keys(CONFIG.Canvas.lightAnimations);
	  for (const animation of animations) {
		const label = game.i18n.localize(CONFIG.Canvas.lightAnimations[animation].label);
		const option = new Option(label, animation);
		animationSelect.append(option);
	  }

	  // Sélection de l'animation actuelle du modèle par défaut (par exemple celui défini dans getData)
	  const selectedModel = AgnosticLight.models[this.object.selectedLightKey];
	  if (selectedModel && selectedModel.animation) {
		animationSelect.val(selectedModel.animation.type || "none");
	  }
	}
  
  
  

  async _updateObject(event, formData) {
	const lightSelect = this.form.querySelector("#light-select");
	let model_data = AgnosticLight.models[lightSelect.value];
	console.log(formData);
	
    // Mettez à jour les paramètres de la torche
    const updatedLight = {
      color: formData.color,
      intensity: parseFloat(formData.intensity),
      dim: parseInt(formData.dim, 10),
      bright: parseInt(formData.bright, 10),
	  angle: parseInt(formData.angle, 10),
      animation: {type: formData.animation, speed:model_data.animation.speed},
      //vision: formData.vision
    };
	// Mettre à jour le modèle avec les nouvelles valeurs
	Object.assign(model_data, updatedLight);

	// Mise à jour du modèle global
	AgnosticLight.models[lightSelect.value] = model_data;
	saveLightModels()
    //await game.settings.set("agnostic-light", "torchLight", torchSettings);
    // Mettez à jour l'exemple de paramètre
    await game.settings.set("agnostic-light", "tokenButton", formData.tokenButton);
    await game.settings.set("agnostic-light", "lightPresets", formData.lightPresets);
    await game.settings.set("agnostic-light", "al_vision", formData.al_vision);
  }
}

// Enregistrement des paramètres de module
Hooks.once('init', async function() {
  console.log("agnostic-light | registering the control panel");
  
  // Enregistrer le paramètre lightModels
    game.settings.register("agnostic-light", "lightModels", {
        name: "Light Models", // Nom descriptif (non utilisé ici car config: false)
        hint: "Stores the configuration for all light models.",
        scope: "world", // Accessible à tous les joueurs dans le monde
        config: false, // Non visible dans l'interface utilisateur
        type: Object, // Type des données stockées
        default: {
			torchLight: {
				name: "agnostic-light.ui.torchLight",
				dim: 12,
				bright: 6,
				color: "#fab87a",
				angle: 360,
				alpha: 0.5,
				animation: { type: "LIGHT.AnimationFlame", speed: 5, intensity: 5 },
			},
			lamp: {
				name: "agnostic-light.ui.lamp",
				dim: 12,
				bright: 9,
				color: "#ffa200",
				angle: 360,
				alpha: 0.5,
				animation: { type: "torch", speed: 3, intensity: 3 },
			},
			bullseye: {
				name: "agnostic-light.ui.bullseye",
				dim: 24,
				bright: 18,
				color: "#ffa200",
				angle: 45,
				alpha: 0.5,
				animation: { type: "torch", speed: 3, intensity: 3 },
				intensity: 0.5,
			},
			hoodedOpen: {
				name: "agnostic-light.ui.hoodedOpen",
				dim: 24,
				bright: 10,
				color: "#ffa200",
				angle: 360,
				alpha: 0.5,
				animation: { type: "torch", speed: 3, intensity: 3 },
				intensity: 0.5,
			},
			hoodedClosed: {
				name: "agnostic-light.ui.hoodedClosed",
				dim: 5,
				bright: 0,
				color: "#ffa200",
				angle: 360,
				alpha: 0.5,
				animation: { type: "torch", speed: 3, intensity: 3 },
				intensity: 0.5,
			},
			lightcantrip: {
				name: "agnostic-light.ui.lightcantrip",
				dim: 14,
				bright: 7,
				color: "#fffab8",
				angle: 360,
				alpha: 0.5,
				animation: { type: "torch", speed: 2, intensity: 1 },
				intensity: 0.5,
			},
			moontouched: {
				name: "agnostic-light.ui.moontouched",
				dim: 30,
				bright: 15,
				color: "#38c0f3",
				angle: 360,
				alpha: 0.5,
				animation: { type: "torch", speed: 1, intensity: 1 },
				intensity: 0.5,
			},
			sunlight: {
				name: "agnostic-light.ui.sunlight",
				dim: 60,
				bright: 30,
				color: "#fff45c",
				angle: 360,
				alpha: 0.6,
				animation: { type: "torch", speed: 1, intensity: 5 },
				intensity: 0.5,
			},
		}
    });
  
  
  
  game.settings.register("agnostic-light", "tokenButton", {
    name: game.i18n.localize("agnostic-light.controlPanel.tokenButton"),
    hint: game.i18n.localize("agnostic-light.controlPanel.tokenButtonhint"),
    scope: "world",
    config: true,
    type: Boolean,
    default: true
  });
  
  game.settings.register("agnostic-light", "lightPresets", {
    name: game.i18n.localize("agnostic-light.controlPanel.lightPresets"),
    hint: game.i18n.localize("agnostic-light.controlPanel.lightPresetshint"),
    scope: "world",
    config: true,
    type: Boolean,
    default: true
  });
  
  game.settings.register("agnostic-light", "al_vision", {
    name: game.i18n.localize("agnostic-light.controlPanel.al_vision"),
    hint: game.i18n.localize("agnostic-light.controlPanel.al_visionhint"),
    scope: "world",
    config: true,
    type: Boolean,
    default: true
  });
  
  // Register the new setting with a select option
	game.settings.register("agnostic-light", "lightIcon", {
		name: game.i18n.localize("agnostic-light.controlPanel.lightIcon"),
		hint: game.i18n.localize("agnostic-light.controlPanel.lightIcon"),
		scope: "world",
		config: true,
		type: String,
		choices: {
			"feu": game.i18n.localize("agnostic-light.controlPanel.lightIconfeu"),
			"lampe": game.i18n.localize("agnostic-light.controlPanel.lightIconlampe")
		},
		default: "feu"
	});

	game.settings.registerMenu("agnostic-light", "openControlPanel", {
		name: game.i18n.localize("agnostic-light.controlPanel.torchSettings"),
		label: game.i18n.localize("agnostic-light.controlPanel.config"),
		hint: game.i18n.localize("agnostic-light.controlPanel.confighint"),
		icon: "fa-solid fa-flashlight",
		type: ControlPanel,
		restricted: true
	  });


});

Hooks.once('ready', async () => {
    console.log("agnostic-light | Initializing objects...");

    // Charger les modèles sauvegardés ou utiliser les valeurs par défaut
    const storedModels = game.settings.get("agnostic-light", "lightModels") || {};

    // Créer l'objet central pour les lumières
    window.AgnosticLight = {
        models: storedModels,
        applyLight(token, lightKey) {
            const settings = this.models[lightKey];
            if (settings) {
                token.document.update({
                    light: {
                        dim: settings.dim,
                        bright: settings.bright,
                        color: settings.color,
                        angle: settings.angle,
                        animation: settings.animation,
                    },
                });
                token.document.setFlag('agnostic-light', 'lightIconState', 'on');
            }
        },
        resetLight(token) {
            const oldLight = token.document.getFlag('agnostic-light', 'base_light');
            token.document.update({ light: oldLight });
            token.document.setFlag('agnostic-light', 'lightIconState', 'off');
        },
    };

    console.log("agnostic-light | Initialized models:", window.AgnosticLight.models);
});

Handlebars.registerHelper('eq', function(a, b) {
    return a === b;
});

function saveLightModels() {
    game.settings.set("agnostic-light", "lightModels", AgnosticLight.models)
	.then(() => {
		console.log("Agnostic Light | Models saved:", AgnosticLight.models);
	}).catch((error) => {
		console.error("Agnostic Light | Error saving models:", error);
	});		
}