### Agnostic Light
This package adds, light and vision control in the UI for the GM and players to quickly light up torches, get blinded and the like. It is system agnostic for those of us who like to play smaller, less well-known RPGs.

![Screenshot](https://gitlab.com/jello195/agnostic-light/raw/master/assets/light_onoff.gif)
![Screenshot](https://gitlab.com/jello195/agnostic-light/raw/master/assets/light_onoff_diff.gif)
![Screenshot](https://gitlab.com/jello195/agnostic-light/raw/master/assets/panel_onoff.gif)

## Authors
[Jello195](https://gitlab.com/jello195)
<details> 
  <summary>Wanna support me? </summary>
   Here come this ebegging ಥ_ಥ
</details>
<a href="https://ko-fi.com/jello195"><img src="https://www.ko-fi.com/img/githubbutton_sm.svg" alt="Support me on ko-fi"></img></a>

## Supported languages
- French
- English
- German
- Spanish
- Italian
- Russian

## Known issues
- Cannot implement the portugues version for some unknown reason (still looking into it).